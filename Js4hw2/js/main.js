function Hamburger(size, stuffing) {
    if (!this._arrStuffing) {
        this._arrStuffing = [];
    }
    try {
        if (size) {
            this._size = size;

        } else {
            throw new HamburgerException('Выберите розмер блюда');

        }
        if (stuffing) {
            this._arrStuffing.push(stuffing);

        } else {
            throw new HamburgerException('Выберите добавку блюда');

        }
    } catch (e) {
        console.log(e.name+" "+e.message);

    }

}

Hamburger.SIZE_SMALL = {
    size: 'small',
    prise: 50,
    kcal: 20
};
Hamburger.SIZE_LARGE = {
    size: 'large',
    prise: 100,
    kcal: 40
};
Hamburger.STUFFING_SALAD = {
    name: 'salad',
    prise: 20,
    kcal: 5
};
Hamburger.STUFFING_SALAD = {
    name: 'potato',
    prise: 15,
    kcal: 10
};
Hamburger.TOPPING_MAYO = {
    name: 'mayo',
    prise: 20,
    kcal: 5
};
Hamburger.TOPPING_SPICE = {
    name: 'spice',
    prise: 15,
    kcal: 0
};
Hamburger.STUFFING_CHEESE = {
    name: 'cheese',
    prise: 10,
    kcal: 20
};

function HamburgerException(message) {
    this.message = message;
    this.name = HamburgerException;
};

Hamburger.prototype.getSize = function () {
    return ('Размер вашего бургера'+" "+ this._size.size );
}


Hamburger.prototype.addTopping = function (topping) {
    if (!this._arrTopping) {
        this._arrTopping = [];
    }
    try {
        if (!topping) {
            throw new HamburgerException('Выберите один из топингов')
        }
        if (this._arrTopping.indexOf(topping) !== -1) {
            throw new HamburgerException('топинг добавлен ранее')
        }

        this._arrTopping.push(topping)

    } catch (e) {
        console.log(e.name+" "+e.message)
    }

};

Hamburger.prototype.getTopping = function () {
    var arrToppingName = []
    this._arrTopping.forEach(elem => {
        arrToppingName.push(elem.name)
    });
    return ("В вашем бургере такие топинги:" +" " +arrToppingName)
};

Hamburger.prototype.removeTopping = function (topping) {
    try {
        if (this._arrTopping.indexOf(topping) === -1) {

            throw new HamburgerException('Нет такого топинга')
        }
        this._arrTopping.splice(this._arrTopping.indexOf(topping), 1)

    } catch (e) {
        console.log(`${e.name} ${e.message}`);

    }
}


Hamburger.prototype.addStuffing = function (stuffing) {
    try {
        if (this._arrStuffing.includes(stuffing)) {
            throw new HamburgerException('начинка добавлена ранее')
        }
        this._arrStuffing.push(stuffing)
    } catch (e) {
        console.log(e.name+" "+e.message);
    }

}

Hamburger.prototype.getStuffing = function () {
    var arrStuffingName = [];
    this._arrStuffing.forEach(elem => {
        arrStuffingName.push(elem.name)
    });
    return ('В вашем бургере такие начинки :'+" "+arrStuffingName);
}

Hamburger.prototype.calculateCalories = function () {
    var summKcalTopping = this._size.kcal;
    var initialValue = 0;
    for (key in this) {
        if (key === "_arrStuffing") {
            var summ = this._arrStuffing.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue.kcal
            }, initialValue);
            summKcalTopping += summ
        }
        if (key === "_arrTopping") {
            var summ = this._arrTopping.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue.kcal
            }, initialValue);
            summKcalTopping += summ
        }
    }

    return ('В вашем бургере'+" "+ summKcalTopping+" "+ 'kcal')
}

Hamburger.prototype.calculatePrice = function () {
    var prise = this._size.prise;
    var initialValue = 0;

        for (const key in this) {
        if (key === "_arrStuffing") {
            var summ = this._arrStuffing.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue.prise
            }, initialValue);
            prise += summ
        }
        if (key === "_arrTopping") {
            var summ = this._arrTopping.reduce(function (accumulator, currentValue) {
                return accumulator + currentValue.prise
            }, initialValue);
            prise += summ
        }
    }

    return ('Цена вашего бургера'+" " +prise +" "+'UE')
}


var ham = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE);

ham.addTopping(Hamburger.TOPPING_MAYO)
ham.addTopping(Hamburger.TOPPING_SPICE)
ham.removeTopping(Hamburger.TOPPING_MAYO)
ham.addStuffing(Hamburger.STUFFING_POTATO)
ham.addStuffing(Hamburger.STUFFING_SALAD)

console.log(ham);

console.log(ham.getTopping());
console.log(ham.getStuffing());
console.log(ham.getSize());
console.log(ham.calculateCalories());
console.log(ham.calculatePrice());