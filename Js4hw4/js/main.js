const container = document.createElement('div');
const table = document.createElement('table');

for (let i = 0; i < 30; i++) {
    let tr = document.createElement('tr')
    table.appendChild(tr);
    for (let i = 0; i < 30; i++) {
        tr.appendChild(document.createElement('td'));
    }
}
container.setAttribute('class', 'container');
document.body.appendChild(container);
container.appendChild(table);

let body = document.getElementsByTagName('body');
for (let i = 0; i < body.length; i++) {
    body[i].onclick = function (event) {
        if (event.target.tagName === "TD") {
            event.target.classList.toggle('active')
        }
        if (event.target === this) {
            table.classList.toggle("table")
        }
    }
}