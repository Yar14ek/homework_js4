class HamburgerException extends Error {
    constructor(message) {
        super(message)
        this.message = message;
        this.name = 'Hamburger_Exception';
    }
};

class Hamburger {
    constructor(size, stuffing) {
        if (!this.arrStuffing) {
            this._arrStuffing = [];
        }
        try {
            if (size == Hamburger.SIZE_SMALL || size == Hamburger.SIZE_LARGE) {
                this._size = size;
            } else {
                throw new HamburgerException(`${size} Выберите размер сендвича из списка`);
            }
            if (stuffing == Hamburger.STUFFING_SALAD || stuffing == Hamburger.STUFFING_POTATO || stuffing == Hamburger.STUFFING_CHEESE) {
                this._arrStuffing.push(stuffing)
            } else {
                throw new HamburgerException(`${stuffing} Выберите начинку из списка`);
            }

        } catch (e) {
            console.log(`${e.name} ${e.message}`);

        }
    };

    get size() {
        if (!this._size) {
            throw new HamburgerException(`Розмер бургера не выбран`);
        }
        return ('Размер вашего бургера' + " " + this._size.size);
    }


    set topping(topping) {
        if (!this._arrTopping) {
            this._arrTopping = [];
        }
        try {
            if (!topping) {
                throw new HamburgerException('Выберите один из топингов')
            }
            if (this._arrTopping.indexOf(topping) !== -1) {
                throw new HamburgerException('топинг добавлен ранее')
            }
            this._arrTopping.push(topping)

        } catch (e) {
            console.log(`${e.name}  ${e.message}`)
        }
    }

    get topping() {
        var arrToppingName = []
        this._arrTopping.forEach(elem => {
            arrToppingName.push(elem.name)
        });
        return ("В вашем бургере такие топинги:" + " " + arrToppingName)


    }


    set stuffing(stuffing) {
        try {
            if (this._arrStuffing.indexOf(stuffing) === -1) {
                this._arrStuffing.push(stuffing)
            } else {

                throw new HamburgerException(`начинка добавлена ранее`)
            }
        } catch (e) {
            console.log(`${e.name} ${e.message}`)
        }
    }

    set removeTopping(topping) {
        try {
            if (this._arrTopping.indexOf(topping) === -1) {

                throw new HamburgerException(`Нет такого топинга`)
            }
            this._arrTopping.splice(this._arrTopping.indexOf(topping), 1)

        } catch (e) {
            console.log(`${e.name} ${e.message}`);

        }
    }

    get stuffing() {
        let arrStuffingName = [];
        this._arrStuffing.forEach(elem => {
            arrStuffingName.push(elem.name)
        });
        return (`В вашем бургере такие начинки :${arrStuffingName}`);

    }

    get calculateCalories() {
        let sizeKcal = this._size.kcal

        let stuffKcal = this._arrStuffing.reduce(function (accumulator, currentValue) {
            return accumulator + currentValue.kcal
        }, 0);

        let topingKcal = this._arrTopping.reduce(function (accumulator, currentValue) {
            return accumulator + currentValue.kcal
        }, 0);
        return sizeKcal + topingKcal + stuffKcal
    }

    get calculatePrice() {
        let prise = this._size.prise;

        let stuffPrise = this._arrStuffing.reduce(function (accumulator, currentValue) {
            return accumulator + currentValue.prise
        }, 0);

        let topingPrise = this._arrTopping.reduce(function (accumulator, currentValue) {
            return accumulator + currentValue.prise
        }, 0);

        return prise + stuffPrise + topingPrise
    }


    static SIZE_SMALL = {
        size: 'small',
        prise: 50,
        kcal: 20
    };

    static STUFFING_POTATO = {
        name: 'potato',
        prise: 15,
        kcal: 10
    };

    static SIZE_LARGE = {
        size: 'large',
        prise: 100,
        kcal: 40
    };
    static STUFFING_SALAD = {
        name: 'salad',
        prise: 20,
        kcal: 5
    };
    static TOPPING_MAYO = {
        name: 'mayo',
        prise: 20,
        kcal: 5
    };
    static TOPPING_SPICE = {
        name: 'spice',
        prise: 15,
        kcal: 0
    };
    static STUFFING_CHEESE = {
        name: 'cheese',
        prise: 10,
        kcal: 20
    };
}


let sandwich = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
console.log(sandwich.size);
sandwich.topping = Hamburger.TOPPING_SPICE;
sandwich.stuffing = Hamburger.STUFFING_CHEESE;
console.log(sandwich.topping);
console.log(sandwich.calculateCalories);
console.log(sandwich.calculatePrice);

console.log(sandwich);